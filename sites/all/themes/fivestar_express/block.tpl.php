<?php
// $Id$

/*
+----------------------------------------------------------------+
|   Fivestar Express for Dupal 6.x - Version 1.0                 |
|   Copyright (C) 2009 Antsin.com All Rights Reserved.           |
|   @license - Copyrighted Commercial Software                   |
|----------------------------------------------------------------|
|   Theme Name: Fivestar Express                                 |
|   Description: Fivestar Express by Antsin                      |
|   Author: Antsin.com                                           |
|   Website: http://www.antsin.com/                              |
|----------------------------------------------------------------+
|   This file may not be redistributed in whole or               |
|   significant part.                                            |
+----------------------------------------------------------------+
*/ 
?>

<div id="block-<?php print $block->module .'-'. $block->delta. '_'.$block->region; ?>" class="block block-<?php print $block->module ?> <?php if (function_exists(block_class)) print block_class($block); ?>">
 
  <?php if (($block->region =='top') || ($block->region =='left') || ($block->region =='right') || ($block->region =='showcase')): ?>
    <div class="top-left"><div class="top-right"><div class="top"></div></div></div>
  <?php endif; ?>

  <div class="block-inner clearfix">
    <?php if ($block->subject): ?>
      <h2 class="title"><?php print $block->subject; ?></h2>
    <?php endif; ?>
	<div class="content">
      <?php print $block->content; ?>
    </div>
	<?php print $edit_links; ?>
  </div>

  <?php if (($block->region =='top') || ($block->region =='left') || ($block->region =='right') || ($block->region =='showcase')): ?>
    <div class="bottom-left"><div class="bottom-right"><div class="bottom"></div></div></div>
  <?php endif; ?>

</div> <!-- /block-inner, /block -->
