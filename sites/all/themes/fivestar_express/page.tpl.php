<?php
// $Id$

/*
+----------------------------------------------------------------+
|   Fivestar Express for Dupal 6.x - Version 1.0                 |
|   Copyright (C) 2009 Antsin.com All Rights Reserved.           |
|   @license - Copyrighted Commercial Software                   |
|----------------------------------------------------------------|
|   Theme Name: Fivestar Express                                 |
|   Description: Fivestar Express by Antsin                      |
|   Author: Antsin.com                                           |
|   Website: http://www.antsin.com/                              |
|----------------------------------------------------------------+
|   This file may not be redistributed in whole or               |
|   significant part.                                            |
+----------------------------------------------------------------+
*/ 
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language; ?>" lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>">

<head>
  <title><?php print $head_title; ?></title>
  <?php print $head; ?>
  <?php print $styles; ?> 
  <?php print $scripts; ?>

</head>
<body class="<?php print $body_classes; ?>">
  <div id="page"><div id="page-inner">
    <div id="secondary">
      <?php if ($secondary_links): ?>
        <div id="secondary-inner">
          <?php print theme('links', $secondary_links); ?>
        </div>
      <?php endif; ?>
	</div> <!-- /#secondary -->
    <div id="header"><div id="header-inner" class="clearfix">
      <?php if ($logo || $site_name || $site_slogan): ?>
        <div id="logo-title">
          <?php if ($logo): ?>
            <div id="logo"><a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" id="logo-image" /></a></div>
          <?php endif; ?>

		  <div id="site-name-slogan">
            <?php if ($site_name): ?>   
              <h1 id="site-name">
                <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><?php print $site_name; ?></a>
              </h1>
            <?php endif; ?>
            <?php if (($site_slogan) && ($site_name)): ?>
              <div id="site-slogan"><?php print $site_slogan; ?></div>
            <?php endif; ?>
			<?php if (($site_slogan) && (!$site_name)): ?>
              <div id="site-slogan-noname"><?php print $site_slogan; ?></div>
            <?php endif; ?>
		  </div>
        </div> <!-- /#logo-title -->
      <?php endif; ?>

      <?php if ($header): ?>
        <div id="header-blocks" class="region region-header">
          <?php print $header; ?>
        </div> <!-- /#header-blocks -->
      <?php endif; ?>

    </div></div> <!-- /#header-inner, /#header -->

	<?php if ($primary_links || $navbar): ?>
      <div id="navbar"><div id="navbar-inner">
		<?php if ($search): ?>
          <div id="search"><?php print $search; ?></div>
	    <?php endif; ?>
        <?php if ($primary_links): ?>
          <div id="primary" class="clearfix">
            <?php print menu_tree($menu_name = 'primary-links'); ?>
	      </div>
        <?php endif; ?> <!-- /#primary -->
        <?php print $navbar; ?>
      </div></div> <!-- /#navbar-inner, /#navbar -->
    <?php endif; ?> 
	
    <div id="main"><div id="main-inner" class="clearfix">
      <div id="content"><div id="content-inner">
	    <?php if ($headline || $subheadline): ?>
          <div id="headlines" class="clearfix">
		    <div class="top-left"><div class="top-right"><div class="top"></div></div></div>
	        <div id="headlines-inner" class="<?php print $headlines;?> clearfix">
              <?php if ($headline): ?>
                <div id="headline" class="column">
                  <?php print $headline; ?>
                </div><!-- /headline -->
              <?php endif; ?>
              <?php if ($subheadline): ?>
                <div id="subheadline" class="column">
                  <?php print $subheadline; ?>
                </div><!-- /subheadline -->
              <?php endif; ?>
		    </div>
		  <div class="bottom-left"><div class="bottom-right"><div class="bottom"></div></div></div>	
	      </div><!-- /#headline-inner, /#headline -->
        <?php endif; ?>

	    <?php if ($showcase): ?>
          <div id="showcase" class="clearfix">
	        <div id="showcase-inner">
              <?php print $showcase; ?>
		    </div>
	      </div><!-- /#showcase-inner, /#showcase -->
        <?php endif; ?>

        <?php if ($mission): ?>
          <div id="mission"><?php print $mission; ?></div>
        <?php endif; ?>
        
	    <div class="top-left"><div class="top-right"><div class="top"><div id="breadcrumb" class="clearfix"><?php print $breadcrumb; ?></div></div></div></div>
		  <div id="content-area">
		    <?php if ($content_top): ?>
              <div id="content-top" class="clearfix">
                <?php print $content_top; ?>
              </div><!-- /content_top -->
            <?php endif; ?>   
		    <?php if ($title || $tabs || $help || $messages): ?>
              <div id="content-header">
                <?php if ($title): ?>
                  <h1 class="title"><?php print $title; ?></h1>
                <?php endif; ?>
                <?php print $messages; ?>
                <?php if ($tabs): ?>
                  <div class="tabs"><?php print $tabs; ?></div>
                <?php endif; ?>
                <?php print $help; ?>
              </div> <!-- /#content-header -->
            <?php endif; ?>
            <?php print $content; ?>
            <?php if ($content_bottom): ?>
              <div id="content-bottom" class="clearfix">
                <?php print $content_bottom; ?>
              </div><!-- /content_bottom -->
            <?php endif; ?>   
          </div>
	    <div class="bottom-left"><div class="bottom-right"><div class="bottom"></div></div></div>		
      </div></div> <!-- /#content-inner, /#content -->

	  <?php if ($top || $left || $right): ?>
        <div id="sidebar">
          <?php if (($top && $left && $right) || ($top && !$left && !$right)): ?>
            <div id="sidebar-top"><div id="sidebar-top-inner" class="region region-top">
              <?php print $top; ?>
            </div></div> <!-- /#sidebar-top-inner, /#sidebar-top -->
          <?php endif; ?>
          <?php if ($left): ?>
            <div id="sidebar-left"><div id="sidebar-left-inner" class="region region-left">
              <?php print $left; ?>
            </div></div> <!-- /#sidebar-left-inner, /#sidebar-left -->
          <?php endif; ?>

          <?php if ($right): ?>
            <div id="sidebar-right"><div id="sidebar-right-inner" class="region region-right">
              <?php print $right; ?>
            </div></div> <!-- /#sidebar-right-inner, /#sidebar-right -->
          <?php endif; ?>
	    </div>
	  <?php endif; ?>

    </div></div> <!-- /#main-inner, /#main -->

    <?php if ($footer_one || $footer_two || $footer_three || $footer_four): ?>
      <div id="footer"><div id="footer-inner" class="<?php print $footer; ?> region-footer clearfix">	  	
        <?php if ($footer_one): ?>
          <div id="footer-one" class="column">
            <?php print $footer_one; ?>
          </div><!-- /footer-one -->
        <?php endif; ?>
        <?php if ($footer_two): ?>
          <div id="footer-two" class="column">
            <?php print $footer_two; ?>
          </div><!-- /footer-two -->
        <?php endif; ?>
		<?php if ($footer_three): ?>
          <div id="footer-three" class="column">
            <?php print $footer_three; ?>
          </div><!-- /footer-three -->
        <?php endif; ?>
	    <?php if ($footer_four): ?>
          <div id="footer-four" class="column">
            <?php print $footer_four; ?>
          </div><!-- /footer-four -->
        <?php endif; ?>
      </div></div> <!-- /#footer-inner, /#footer -->
    <?php endif; ?>
  </div></div> <!-- /#page-inner, /#page -->

  <div id="closure"><div id="closure-inner" class="region region-closure"><div id="designed-by"><small><a href="http://www.antsin.com" target="blank">Designed by Antsin.com</a></small></div><?php print $closure_region; ?></div></div>

  <?php print $closure; ?>

</body>
</html>
