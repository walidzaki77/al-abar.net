<?php
// $Id$

/*
+----------------------------------------------------------------+
|   Fivestar Express for Dupal 6.x - Version 1.0                 |
|   Copyright (C) 2009 Antsin.com All Rights Reserved.           |
|   @license - Copyrighted Commercial Software                   |
|----------------------------------------------------------------|
|   Theme Name: Fivestar Express                                 |
|   Description: Fivestar Express by Antsin                      |
|   Author: Antsin.com                                           |
|   Website: http://www.antsin.com/                              |
|----------------------------------------------------------------+
|   This file may not be redistributed in whole or               |
|   significant part.                                            |
+----------------------------------------------------------------+
*/ 
?>

<?php if (!empty($title)) : ?>
  <h3><?php print $title; ?></h3>
<?php endif; ?>
<div class="views-view-grid">
  <?php foreach ($rows as $row_number => $columns): ?>
    <?php
      $row_class = 'row-' . ($row_number + 1);
      if ($row_number == 0) {
        $row_class .= ' row-first';
      }
      elseif (count($rows) == ($row_number + 1)) {
        $row_class .= ' row-last';
      }
    ?>
    <div class="row <?php print $row_class; ?> clearfix">
      <?php foreach ($columns as $column_number => $item): ?>
        <div class="column <?php print 'col-'. ($column_number + 1); ?>">
          <?php print $item; ?>
        </div>
      <?php endforeach; ?>
    </div>
  <?php endforeach; ?>
</div>
