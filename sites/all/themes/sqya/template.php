<?php
// $Id: template.php,v 1.2.2.2 2009/04/25 06:19:22 hswong3i Exp $

/**
 * Return a themed mission trail.
 *
 * @return
 *   a string containing the mission output, or execute PHP code snippet if
 *   mission is enclosed with <?php ?>.
 */
function phptemplate_mission() {
  $mission = theme_get_setting('mission');
  if (preg_match('/^<\?php/', $mission)) {
    $mission = drupal_eval($mission);
  }
  else {
    $mission = filter_xss_admin($mission);
  }
  return isset($mission) ? $mission : '';
}

/**
 * Generates IE CSS links for LTR and RTL languages.
 */
function _phptemplate_variables($hook, $vars) {
  if ($hook == 'page') {
    drupal_add_js(path_to_theme() .'/scripts.js', 'theme');
    $vars['scripts'] = drupal_get_js();
    return $vars;
  }
  return array();
}