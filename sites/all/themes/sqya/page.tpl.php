﻿<?php
// $Id: page.tpl.php,v 1.18.2.1 2009/04/30 00:13:31 goba Exp $
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">

<head>
<title><?php print $head_title; ?></title>
  <?php print $head; ?>
  <?php print $styles; ?> 
  <?php print $scripts; ?>
  <!--[if lte IE 6]> <link rel="stylesheet" type="text/css" href="/sqya/sites/all/themes/sqya/styles/style_ie6.css" /><![endif]-->
<!--[if lte IE 7]> <link rel="stylesheet" type="text/css" href="/sqya/sites/all/themes/sqya/styles/style_ie7.css" /><![endif]-->


  
</head>

<body>

<div id="container" class="center">
	<!-- the header start-->
	<div class="header  block-1">
		<a href="http://sqya.net" class="logo">
&nbsp;
	</a>
		<div id="slideShow">
			<div id="slideShowItems">
				<div>
					<img alt="" src="http://os4a.com/sqya/sites/all/themes/sqya/images/slide1.png" border="0" /> </div>
				<div>
					<img alt="" src="http://os4a.com/sqya/sites/all/themes/sqya/images/slide2.png" border="0" /> </div>
			</div>
		</div>
		<div class="news"><marquee  direction="right" onmouseover="stop();" onmouseout="start();"><?php 
global $user;
$uid=$user->uid;
$resource=db_query("SELECT  node_revisions.body AS node_revisions_body
 FROM node node 
 LEFT JOIN content_field_news_users node_data_field_news_users ON node.vid = node_data_field_news_users.vid
 LEFT JOIN users users_node_data_field_news_users ON node_data_field_news_users.field_news_users_uid = users_node_data_field_news_users.uid
 LEFT JOIN node_revisions node_revisions ON node.vid = node_revisions.vid
 WHERE (node.type in ('news')) AND (users_node_data_field_news_users.uid ='$uid') ORDER BY node_revisions_body DESC");
$results = array();
while ($row = db_fetch_array($resource))
{
echo "  ";
echo $row['node_revisions_body'];
echo "  ";
echo "<img src='http://sqya.net/sites/all/themes/mpFREE/news_logo.png' height='15'>";

} 
//$results[] = $row;

// show the results

//echo "<marquee>" . print_r($results, TRUE) . "</marquee>";

?></marquee>

		</div>
	</div>
	<!-- the header end-->
	<!-- the navigation bar start -->
	
	<div class="nav  block-1">
		<?php if (isset($primary_links)) : ?>
          <?php print theme('links', $primary_links, array('class' => 'sf-menu')) ?>
        <?php endif; ?>
	</div>
	<!-- the navigation bar end-->
	<!-- the navigation bar start-->
	<div class="clear">
	</div>
	
	<!-- block start-->
	        <div class="block  block-3-restof top-title">
			  <?php if ($title): print '<h2 class="title'. ($tabs ? ' with-tabs' : '') .'">'. $title .'</h2>'; endif; ?>
    <?php if ($tabs): print '<div class="tabs">'. $tabs .'</div>'; endif; ?>
    <?php if ($show_messages && $messages): print $messages; endif; ?> 
    <?php print $help ?>
                <?php print $content ?>
				
            </div>
			<?php if ($right): ?>
            <div class="block block-3-padding-border top-title block-title">
            <?php print $right ?>	
            </div>
			<?php endif ?>
            <div class="clear">
            </div>



            <div class="clear">
            </div>


<!-- footer start-->

<div id="footer">
	<center>	<?php print $footer ?></center>
</div>
<!-- footer end-->
</body>

</html>
