Module: Option Trim
Author: Chris Yu
Sponsor: Classic Graphics (http://www.classicgraphics.com)

This module provides a way for trimming options on CCK select fields, either via
a form alter of the initial options if the trim is static or via ajax if the trim
is dynamic and dependent on a parent field.

Currently supported CCK types are:
Text
Number
Node Reference
User Reference
Role Reference

BUGS
----
http://drupal.org/project/issues/option_trim
