// $Id: nodetable.js,v 1.1 2010/08/14 20:32:01 zserno Exp $
Drupal.behaviors.nodeTable = function (context) {
  var settings = Drupal.settings.nodeTable;
  var headersObj = new Object;
  $.each(settings.headerCells, function (index, value) {
    // Check if current column is not sortable.
    if (!value) {
      headersObj[index] = {sorter: false};
    }
    else {
      headersObj[index] = {sorter: 'integer'};
    }
  });

  // Prevent operation links from being sortable.
  var length = settings.headerCells.length;
  headersObj[length] = {sorter: false};
  headersObj[length + 1] = {sorter: false};

  // Remove even/odd classes (added by Drupal core) from rows
  // to avoid conflict with the tablesorter plugin.
  $('#' + settings.id + ' tr').removeClass('even odd');
  $('#' + settings.id).tablesorter({sortList: [[settings.defaultSort, settings.defaultSortOrder]], headers: headersObj, widgets: ['zebra'],  widgetZebra: {css: ['nodetable-odd', 'nodetable-even']}, debug: false});
};

