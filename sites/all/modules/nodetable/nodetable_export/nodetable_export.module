<?php
// $Id: nodetable_export.module,v 1.1 2010/08/14 20:32:01 zserno Exp $

/**
 * @file
 * CSV exporter for Nodetable.
 */

/**
 * Implementation of hook_menu().
 */
function nodetable_export_menu() {
  $items = array();

  // @TODO Access control.
  $items['admin/content/nodetable/export/%node'] = array(
    'title' => 'Export table to a CSV file.',
    'page callback' => 'nodetable_export',
    'page arguments' => array(4),
    'access arguments' => array('access tables'),
    'type' => MENU_CALLBACK,
  );

  return $items;
}

/**
 * Export given node's table to CSV file.
 * @param $node
 *   Fully loaded node returned by node_load().
 */
function nodetable_export($node) {
  if (!user_access('access tables')) {
    return;
  }

  $node = node_prepare($node, $teaser);
  $header = $node->table->header;
  $rows = $node->table->rows;
  $filename = 'nodetable_export.csv'; // @TODO: Generate it from node title.

  // Send necessary headers.
  drupal_set_header('Cache-Control: max-age=60, must-revalidate');
  drupal_set_header('Content-type: text/csv; charset=utf-8');
  drupal_set_header('Content-Disposition: attachment; filename="'. $filename .'"');

  // Format header values.
  foreach ($header as &$value) {
    $value = '"'. str_replace('"', '""', decode_entities(strip_tags($value))) .'"';
  }

  // Output table header.
  $comma = t(',');
  print implode($comma, $header) ."\r\n";

  // Format row values.
  foreach ($rows as &$row) {
    foreach ($row as &$cell) {
      $cell = '"'. str_replace('"', '""', decode_entities(strip_tags($cell['data']))) .'"';
    }
    // Output current table row.
    print implode($comma, $row) ."\r\n";
  }
}

/**
 * Implementation of hook_theme().
 */
function nodetable_export_theme() {
  return array(
    'nodetable_export_button' => array(
      'arguments' => array('nid' => NULL),
    ),
  );

}

function theme_nodetable_export_button($nid) {
  $image_path = drupal_get_path('module', 'nodetable_export') . '/images/csv.png';
  $text = t('Export table to CSV.');
  $url = 'admin/content/nodetable/export/'. $nid;
  $url_options = array('html' => TRUE);
  $image = theme('image', $image_path, $text, $text);

  return l($image, $url, $url_options);
}

